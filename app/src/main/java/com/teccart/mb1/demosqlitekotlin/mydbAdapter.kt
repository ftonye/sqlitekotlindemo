package com.teccart.mb1.demosqlitekotlin

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

class mydbAdapter(var mydbHelper: mydbHelper) {
    var bd:SQLiteDatabase
    init {
        this.bd = this.mydbHelper.writableDatabase
    }

    fun getAllStudent():ArrayList<Student>
    {
        var a:ArrayList<Student> = ArrayList<Student>()

        var cursor: Cursor = this.bd.rawQuery("SELECT * FROM STUDENTS;",null)


        var IDINDEX = cursor.getColumnIndex("ID")
        var NOMINDEX = cursor.getColumnIndex("NOM")
        var PRENOMINDEX = cursor.getColumnIndex("PRENOM")
        var AGEINDEX = cursor.getColumnIndex("AGE")

        if((cursor!=null)&& cursor.moveToFirst())
        {
            do
            {
                a.add(Student(cursor.getInt(IDINDEX),cursor.getString(NOMINDEX),cursor.getString(PRENOMINDEX),cursor.getInt(AGEINDEX)))
            }while(cursor.moveToNext())
        }

        return a
    }

    fun addStudent(nom:String,prenom:String,age:Int)
    {
        var contentValues: ContentValues = ContentValues()
        contentValues.put("NOM",nom)
        contentValues.put("PRENOM",prenom)
        contentValues.put("AGE",age)
        this.bd.insert("STUDENTS",null,contentValues)

        //this.bd.execSQL("INSERT INTO STUDENTS(NOM,PRENOM,AGE) VALUES('tata',1);")

    }

    fun UpdateStudentInfo(id:Int,nom:String,prenom:String,age:Int)
    {
        this.bd.execSQL("UPDATE STUDENTS SET NOM='$nom',PRENOM='$prenom',AGE=$age WHERE ID=$id;")

    }

  fun RemoveStudent(id:Int) = this.bd.execSQL("DELETE FROM STUDENTS WHERE ID=$id")



}